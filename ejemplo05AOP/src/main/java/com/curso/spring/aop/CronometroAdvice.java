package com.curso.spring.aop;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

import com.curso.spring.util.Logger;

public class CronometroAdvice implements MethodInterceptor{

	private Logger logger;
	
	@Override
	public Object invoke(MethodInvocation invocation) throws Throwable {
		invocation.getMethod(); // el método
		invocation.getArguments(); // Los Parámetros
		invocation.getThis(); // Target
		
		long inicio = System.currentTimeMillis();
		Object resultado = invocation.proceed();
		
		long fin = System.currentTimeMillis();
		
		String texto = "LLamada a método " + invocation.getMethod().getName() + " de " + invocation.getThis().getClass().getName() + " procesada en " + (fin - inicio) + " milisegundos.";
		System.out.println(texto);
		logger.escribir(texto);
		return resultado;
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

}
