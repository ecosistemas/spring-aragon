package com.curso.spring.aop;

import java.lang.reflect.Method;
import java.time.LocalDateTime;

import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

import com.curso.spring.util.Logger;

@Aspect 
@Component
public class LogAdviceAnotado {

	private Logger logger;
	
	
	
	@Before("within(com.curso.spring.model.business.*)")
	public void before(Method method, Object[] args, Object target) throws Throwable {
		String text = "Llamando al método " + method.getName() + " de " + target.getClass().getName() +". " + LocalDateTime.now();
		System.out.println(text);
		logger.escribir(text);
	}
	@AfterReturning("within(com.curso.spring.model.business.*)")
	public void afterReturning(Object returnValue, Method method, Object[] args, Object target) throws Throwable {
		System.out.println("Fin de la llamada al método " + method.getName() + ". " + LocalDateTime.now());
		
	}
	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	

}
