package com.curso.spring.aop;

import java.lang.reflect.Method;
import java.time.LocalDateTime;

import org.springframework.aop.AfterReturningAdvice;
import org.springframework.aop.MethodBeforeAdvice;

import com.curso.spring.util.Logger;

public class LogAdvice implements MethodBeforeAdvice, AfterReturningAdvice{

	private Logger logger;
	
	
	
	@Override
	public void before(Method method, Object[] args, Object target) throws Throwable {
		String text = "Llamando al método " + method.getName() + " de " + target.getClass().getName() +". " + LocalDateTime.now();
		System.out.println(text);
		logger.escribir(text);
	}
	@Override
	public void afterReturning(Object returnValue, Method method, Object[] args, Object target) throws Throwable {
		System.out.println("Fin de la llamada al método " + method.getName() + ". " + LocalDateTime.now());
		
	}
	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	

}
