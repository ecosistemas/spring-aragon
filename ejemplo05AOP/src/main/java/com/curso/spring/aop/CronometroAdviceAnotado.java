package com.curso.spring.aop;

import org.aopalliance.intercept.MethodInvocation;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import com.curso.spring.util.Logger;

@Aspect
@Component
public class CronometroAdviceAnotado {

	private Logger logger;
	
	@Around("within(com.curso.spring.model.business.*)")
	public Object interceptar(ProceedingJoinPoint invocation) throws Throwable {

		invocation.getSignature().getName(); // Target
		
		long inicio = System.currentTimeMillis();
		Object resultado = invocation.proceed();
		
		long fin = System.currentTimeMillis();
		
		String texto = "LLamada a método " + invocation.getSignature().getName() + " de " + invocation.getThis().getClass().getName() + " procesada en " + (fin - inicio) + " milisegundos.";
		System.out.println(texto);
		logger.escribir(texto);
		return resultado;
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

}
