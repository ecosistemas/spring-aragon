package com.curso.spring;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.curso.spring.model.business.GestorClientes;
import com.curso.spring.model.entity.Cliente;

public class EjemploAOP {
	public static AbstractApplicationContext context = null;

	public static void main(String[] args) {
		context = new ClassPathXmlApplicationContext("applicationContext.xml");
		GestorClientes gestor = context.getBean(GestorClientes.class);
		Cliente cliente = new Cliente();
		gestor.insertar(cliente);
		gestor.borrar(cliente);
		context.close();
	}
}
