package com.curso.spring.model.business;

import com.curso.spring.model.entity.Cliente;

public class GestorClientesImpl implements GestorClientes {

	@Override
	public void insertar(Cliente cliente) {
		System.out.println("Insertando en Gestor clientes -> " + cliente);
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			System.err.println(e.getMessage());
		}

	}

	@Override
	public void borrar(Cliente cliente) {
		System.out.println("Borrando en Gestor clientes -> " + cliente);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			System.err.println(e.getMessage());
		}

	}

}
