package com.curso.spring.model.entity;

public class Cliente {
	
	private String nombre;

	/*
	 * alt+shift+s c Constructor superclass
	 * alt+shift+s o Constructor con campos
	 * alt+shift+s r Constructor getter y setter
	 * alt+shift+s s Constructor toString
	 * 
	 */
	
	@Override
	public String toString() {
		return "Cliente [nombre=" + nombre + ", getNombre()=" + getNombre() + ", getClass()=" + getClass()
				+ ", hashCode()=" + hashCode() + ", toString()=" + super.toString() + "]";
	}

	public Cliente(String nombre) {
		super();
		this.nombre = nombre;
	}

	public Cliente() {
		super();
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
}
