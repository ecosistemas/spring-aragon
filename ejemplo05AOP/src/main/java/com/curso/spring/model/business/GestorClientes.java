package com.curso.spring.model.business;

import com.curso.spring.model.entity.Cliente;

public interface GestorClientes {
	public abstract void insertar(Cliente cliente);
	public abstract void borrar(Cliente cliente);
}
