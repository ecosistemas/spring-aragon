package com.curso.spring.model.business;

public class GestorEmpleados {
	public void insertar(String empleado) {
		System.out.println("Insertando empleado " + empleado + "en el gestor");
		// Negocio
		try {
			Thread.sleep(1500);
		} catch (InterruptedException e) {
			System.err.println(e.getMessage());
		}
	}
	public void borrar(String empleado) {
		System.out.println("Borrando empleado " + empleado + "en el gestor");
		// Negocio
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			System.err.println(e.getMessage());
		}
	}
}
