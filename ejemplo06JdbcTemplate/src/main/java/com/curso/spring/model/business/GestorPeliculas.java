package com.curso.spring.model.business;

import java.util.List;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.curso.spring.model.entity.Pelicula;
import com.curso.spring.model.persistence.IPeliculaDao;

@Service
public class GestorPeliculas implements InitializingBean{
	
	@Autowired
	private IPeliculaDao peliculaDao;
	@Autowired
	public GestorPeliculas() {
		super();
		System.out.println("Creando un gestor de peliculas");
	}
	public int insertar(Pelicula pelicula) {
		
		System.out.println("Lanzando loggers");
		
		return peliculaDao.insertar(pelicula);
	}
	public int modificar(Pelicula pelicula) {
		return peliculaDao.modificar(pelicula);
	}
	public Pelicula buscar(int id) {
		return peliculaDao.buscar(id);
	}
	public List<Pelicula> listar(){
		return peliculaDao.listar();
	}
	public int borrar(int id) {
		return peliculaDao.borrar(id);
	}
	@Override
	public void afterPropertiesSet() throws Exception {
		peliculaDao.createTable();
		
	}
}
