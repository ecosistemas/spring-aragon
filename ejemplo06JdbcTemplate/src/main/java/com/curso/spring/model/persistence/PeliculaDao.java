package com.curso.spring.model.persistence;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.curso.spring.model.entity.Pelicula;

@Repository
public class PeliculaDao implements IPeliculaDao {
	
	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Autowired
	private DaoPeliculaRowMapper peliculaRowMapper;
	
	@Override
	public int insertar(Pelicula pelicula) {
		String query = "insert into peliculas (titulo, director, genero, year) values (?, ?, ?, ?)";
		return jdbcTemplate.update(query,
				pelicula.getTitulo(),
				pelicula.getDirector(),
				pelicula.getGenero(),
				pelicula.getYear());
		
	}

	@Override
	public int modificar(Pelicula pelicula) {
		String query = "update peliculas set titulo=?, director=?, genero=?, year=? where id=?";
		return jdbcTemplate.update(query,
				pelicula.getTitulo(),
				pelicula.getDirector(),
				pelicula.getGenero(),
				pelicula.getYear(),
				pelicula.getId());
	}

	@Override
	public int borrar(int id) {
		String query = "delete from peliculas where id=?";
		
		return jdbcTemplate.update(query,id);
	}

	@Override
	public Pelicula buscar(int id) {
		String query = "select * from peliculas where id=?";
		Pelicula pelicula = null;
		try {
			pelicula = jdbcTemplate.queryForObject(query, peliculaRowMapper,id);
		}catch(EmptyResultDataAccessException e) {
			System.out.println("No existe la pelicula con id: " + id + " -> " + e.getMessage() );
		}
		return pelicula;
	}

	@Override
	public List<Pelicula> listar() {
		String query = "select * from peliculas";
		
		return jdbcTemplate.query(query, peliculaRowMapper);
	}

	@Override
	public void createTable() {
		jdbcTemplate.execute("create table if not exists peliculas ("
				+ "id int NOT NULL AUTO_INCREMENT,"
				+ "titulo varchar(255) NOT NULL,"
				+ "director varchar(255),"
				+ "genero varchar(255),"
				+ "year int,"
				+ "PRIMARY KEY (id)"
				+ ");");
		
	}
	
	
}
