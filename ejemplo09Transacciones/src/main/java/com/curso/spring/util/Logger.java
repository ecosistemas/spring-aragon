package com.curso.spring.util;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

public class Logger implements InitializingBean,DisposableBean{

	private BufferedWriter bw;
	private String nombreFichero;
	public Logger() {
		super();
	}
	public void destroy() throws Exception {
		bw.flush();
		bw.close();
	}
	public void afterPropertiesSet() {
		
		FileWriter fileWriter;
		try {
			fileWriter = new FileWriter(getNombreFichero());
			bw = new BufferedWriter(fileWriter);
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
	}
	public synchronized void escribir(String texto) {
		try {
			bw.write(texto+"\n");
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
	}
	
	public String getNombreFichero() {
		return nombreFichero;
	}
	public void setNombreFichero(String nombreFichero) {
		this.nombreFichero = nombreFichero;
	}
	
	
	
	
}
