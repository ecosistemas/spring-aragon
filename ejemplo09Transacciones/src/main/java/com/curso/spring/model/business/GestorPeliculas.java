package com.curso.spring.model.business;

import java.util.List;



import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.curso.spring.model.entity.Pelicula;
import com.curso.spring.model.persistence.IPeliculaDao;

@Service
public class GestorPeliculas implements InitializingBean{
	
	@Autowired
	private IPeliculaDao peliculaDao;
	@Autowired
	private GestorPeliculas proxyAutoinyectado;
	
	public GestorPeliculas() {
		super();
		System.out.println("Creando un gestor de peliculas");
	}
	/*
	 * REQUIRED = Da soporte a la trans. actual y sino crea una nueva
	 * SUPPORTS = Da soporte a la trans. actual y sino no usa transacciones
	 * MANDATORY = Da soporte a la trans. actual y sino excepción
	 * REQUIRES_NEW = Crea siempre una nueva transaccion, y si ya hay una, la suspende.
	 * NOT_SUPPORTED = No usa transacciones, y si ya hay una, la suspende.
	 * NEVER = No usa transacciones, y si ya hay una, excepción
	 * NESTED = Ejecuta dentro de una transacción si existe una.
	 */
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public int insertar(Pelicula pelicula) {
		
		System.out.println("Lanzando loggers");
		
		return peliculaDao.insertar(pelicula);
	}
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public void insertar(List<Pelicula> peliculas) throws Exception{
		for(Pelicula pelicula: peliculas) {
			proxyAutoinyectado.insertar(pelicula);
		}
	}
	
	public int modificar(Pelicula pelicula) {
		return peliculaDao.modificar(pelicula);
	}
	public Pelicula buscar(int id) {
		return peliculaDao.buscar(id);
	}
	@Transactional(propagation = Propagation.SUPPORTS)
	public List<Pelicula> listar(){
		return peliculaDao.listar();
	}
	public int borrar(int id) {
		return peliculaDao.borrar(id);
	}
	@Override
	public void afterPropertiesSet() throws Exception {
		peliculaDao.createTable();
		
	}
}
