package com.curso.spring.model.persistence;

import java.util.List;

import com.curso.spring.model.entity.Pelicula;

public interface IPeliculaDao {

	int insertar(Pelicula pelicula);
	int modificar(Pelicula pelicula);
	int borrar(int id);
	Pelicula buscar(int id);
	List<Pelicula> listar();
	void createTable();

}