package com.curso.spring.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import com.curso.spring.model.business.GestorPeliculas;
import com.curso.spring.model.persistence.PeliculaDao;
import com.curso.spring.util.Logger;

// Importar otras clases de configuracion
//@Import({JavaConfig.class})
// Importar ficheros xml externos
//@ImportResource("classpath:/configuracion/AppContextDeRefuerzo.xml")

//<context:component-scan base-package="com.curso.spring.model.business"></context:component-scan>
//@ComponentScan(basePackages = "com.curso.spring.model.business")

@Configuration
public class JavaConfig {
	/*
	 * <bean id="dataSource" class="org.springframework.jdbc.datasource.DriverManagerDataSource">
		<property name="driverClassName" value="org.h2.Driver"></property>
		<property name="url" value="jdbc:h2:file:./target/h2/ejemplo03anotaciones"></property>
		<property name="username" value="sa"></property>
		<property name="password" value=""></property>	
	</bean>
	 */
	
	@Bean
	public DataSource dataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName("org.h2.Driver");
		dataSource.setUrl("jdbc:h2:file:./target/h2/ejemplo04JavaConfig");
		dataSource.setUsername("sa");
		dataSource.setPassword("");
		return dataSource;
	}
	/*
	<bean id="logger" class="com.curso.spring.util.Logger">
		<property name="nombreFichero" value="log.txt"></property>
	</bean>*/
	@Bean
	public Logger logger() {
		Logger logger = new Logger();
		logger.setNombreFichero("log.txt");
		return logger;
	}
/*
<bean id="loggerError" class="com.curso.spring.util.Logger">
	<property name="nombreFichero" value="error.txt"></property>
</bean>*/
	@Bean
	public Logger loggerError() {
		Logger logger = new Logger();
		logger.setNombreFichero("error.txt");
		return logger;
	}
	
	// PeliculaDao (sin anotaciones)
	@Bean
	public PeliculaDao peliculaDao() {
		System.out.println("Creando peliculaDao");
		PeliculaDao dao = new PeliculaDao();
		dao.setDataSource(dataSource());
		return dao;
	}
	@Bean
	public GestorPeliculas gestorPeliculas(
			@Autowired PeliculaDao peliculaDao, 
			@Autowired @Qualifier("logger") Logger logger, 
			@Autowired @Qualifier("loggerError") Logger loggerError) {
		GestorPeliculas gestorPeliculas = new GestorPeliculas();
		gestorPeliculas.setLogger(logger);
		gestorPeliculas.setLoggerError(loggerError);
		gestorPeliculas.setPeliculaDao(peliculaDao);
		return gestorPeliculas;
	}
}





















