package com.curso.spring;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

import com.curso.spring.config.JavaConfig;
import com.curso.spring.model.business.GestorPeliculas;
import com.curso.spring.model.entity.Pelicula;

public class EjemploJavaConfig {
public static AbstractApplicationContext context = null;
	
	public static void main(String[] args) {
		context = new AnnotationConfigApplicationContext(JavaConfig.class);
		GestorPeliculas gestorPeliculas = context.getBean(GestorPeliculas.class);
		gestorPeliculas.insertar(new Pelicula());
		context.close();
	}
}
