package com.curso.spring.model.business;

import com.curso.spring.model.entity.Pelicula;
import com.curso.spring.model.persistence.PeliculaDao;
import com.curso.spring.util.Logger;

//@Service
public class GestorPeliculas {
	
	//@Autowired
	private PeliculaDao peliculaDao;
	//@Autowired
	//@Qualifier("logger")
	private Logger logger;
	//@Autowired
	//@Qualifier("loggerError")
	private Logger loggerError;
	
	public GestorPeliculas() {
		super();
		System.out.println("Creando un gestor de peliculas");
	}
	public void setPeliculaDao(PeliculaDao peliculaDao) {
		this.peliculaDao = peliculaDao;
	}
	public void setLogger(Logger logger) {
		this.logger = logger;
	}
	public void setLoggerError(Logger loggerError) {
		this.loggerError = loggerError;
	}
	public void insertar(Pelicula pelicula) {
		// Negocio!
		System.out.println("Lanzando loggers");
		logger.escribir("Lanzando operacion de guardado de pelicula");
		loggerError.escribir("Cuidado!");
		peliculaDao.save(pelicula);
	}
	
	
}
