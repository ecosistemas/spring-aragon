package com.curso.spring;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

import com.curso.spring.config.JavaConfig;
import com.curso.spring.model.entity.Cliente;
import com.curso.spring.model.entity.Pedido;
import com.curso.spring.model.entity.Producto;

public class EjemploJPA {
public static AbstractApplicationContext context = null;
	
	public static void main(String[] args) {
		context = new AnnotationConfigApplicationContext(JavaConfig.class);
		
		EntityManagerFactory emf = context.getBean(EntityManagerFactory.class);
		EntityManager em = emf.createEntityManager();
		
		em.getTransaction().begin();
		Cliente cliente = new Cliente(null, "Ringo", "Calle Falsa 123", "555718239",6000);
		em.persist(cliente);
		System.out.println("Datos del cliente persistido: " + cliente);
		
		Producto productoDeApel = new Producto(null,"Sifon","Apel",10000d,10);
		em.persist(productoDeApel);
		
		Pedido pedido = new Pedido(null, "1234", null, "pagado", cliente, null);
		em.persist(pedido);
		Pedido pedido2 = new Pedido(null, "12342", null, "pagado2", cliente, null);
		em.persist(pedido2);
		
		em.getTransaction().commit();
		em.close();
		
		em = emf.createEntityManager();
		em.getTransaction().begin();
		
		// Ojito con esto, que el merge, si es un update actualiza TODAS las columnas!
		//Cliente clienteAModificar = new Cliente();
		//clienteAModificar.setId(1);
		//clienteAModificar.setTelefono("777");
		//em.merge(clienteAModificar);
		Cliente clienteAModificar = new Cliente(1,"Venancia","C/Falsa 123","1231",6000);
		em.merge(clienteAModificar);
		em.getTransaction().commit();
		em.close();
		
		em = emf.createEntityManager();
		em.getTransaction().begin();
		
		Cliente venancia = em.find(Cliente.class, 1);
		venancia.setTelefono("987654321");
		em.merge(venancia);
		em.getTransaction().commit();
		em.close();
		
		em = emf.createEntityManager();
		em.getTransaction().begin();
		
		Cliente clienteEncontrado1 = em.find(Cliente.class, 1);
		Cliente clienteEncontrado2 = em.find(Cliente.class, 1);
		Cliente clienteEncontrado3 = em.find(Cliente.class, 1);
		
		clienteEncontrado1.setNombre(clienteEncontrado1.getNombre().toUpperCase());
		System.out.println(clienteEncontrado2.getNombre());
		System.out.println(clienteEncontrado3.getNombre());
		
		
		em.getTransaction().commit();
		em.close();
		
		em = emf.createEntityManager();
		em.getTransaction().begin();
		
		
		// Esto no funciona!!!
//		Cliente clienteABorrar = new Cliente();
//		clienteABorrar.setId(2);
//		em.remove(clienteABorrar);
		
//		Cliente clienteEncontradoParaBorrar = em.find(Cliente.class, 1);
//		em.remove(clienteEncontradoParaBorrar);
		
		Cliente clienteABorrar = new Cliente();

		em.persist(clienteABorrar);
		em.remove(clienteABorrar);
		
		
//		Pedido pedidoOjoConEl = em.find(Pedido.class, 1);
//		pedidoOjoConEl.getDetalles();
		
		em.getTransaction().commit();
		em.close();
		
		context.close();
		
	}
}
