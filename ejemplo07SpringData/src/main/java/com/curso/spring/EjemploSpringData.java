package com.curso.spring;

import java.util.List;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

import com.curso.spring.config.JavaConfig;
import com.curso.spring.model.business.GestorClientes;
import com.curso.spring.model.entity.Cliente;

public class EjemploSpringData {
public static AbstractApplicationContext context = null;
	
	public static void main(String[] args) {
		context = new AnnotationConfigApplicationContext(JavaConfig.class);
		GestorClientes gestorClientes = context.getBean(GestorClientes.class);
		gestorClientes.insertar(new Cliente(null,"Ruben","C/Tal de pascual","987654321",1980));
		gestorClientes.insertar(new Cliente(null,"Ruben2","C/Tal de pascual2","987654322",1982));
		gestorClientes.insertar(new Cliente(null,"Ruben3","C/Tal de pascual3","987654323",1983));
		List<Cliente> clientes = gestorClientes.listar();
		for(Cliente cliente: clientes) {
			System.out.println(cliente);
		}
		
		Integer idDelClienteABuscar = clientes.get(clientes.size()-1).getId();
		Cliente clientecillo = gestorClientes.buscar(idDelClienteABuscar);
		System.out.println(clientecillo);
		context.close();
	}
}
