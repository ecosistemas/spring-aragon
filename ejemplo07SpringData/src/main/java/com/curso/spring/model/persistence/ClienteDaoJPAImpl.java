package com.curso.spring.model.persistence;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import com.curso.spring.model.entity.Cliente;

@Repository
public class ClienteDaoJPAImpl implements ClienteDao{
	
	@PersistenceContext //(unitName = "oracle")
	private EntityManager em;

	@Override
	public void insertar(Cliente obj) {
		em.persist(obj);
		
	}

	@Override
	public void modificar(Cliente obj) {
		em.merge(obj);
		
	}

	@Override
	public void borrar(Cliente obj) {
		Cliente clientePersistido = em.find(Cliente.class, obj.getId());
		em.remove(clientePersistido);
		
	}

	@Override
	public Cliente buscar(Integer id) {

		return em.find(Cliente.class, id);
	}

	@Override
	public List<Cliente> listar() {
		TypedQuery<Cliente> query = em.createQuery("select c from Cliente c",Cliente.class);
		return query.getResultList();
	}

}
