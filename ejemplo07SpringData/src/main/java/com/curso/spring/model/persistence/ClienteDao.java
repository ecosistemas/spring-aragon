package com.curso.spring.model.persistence;

import com.curso.spring.model.entity.Cliente;

public interface ClienteDao extends InterfaceDao<Cliente,Integer>{

}
