package com.curso.spring.model.business;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.curso.spring.model.entity.Cliente;
import com.curso.spring.model.persistence.ClienteDao;

@Service
public class GestorClientes {
	
	@Autowired
	private ClienteDao clienteDao;

	public List<Cliente> listar(){
		return clienteDao.listar();
	}

	public Cliente buscar(Integer id) {
		return clienteDao.buscar(id);
	}
	@Transactional
	public void insertar(Cliente cliente) {
		// LN 
		clienteDao.insertar(cliente);
	}
}
