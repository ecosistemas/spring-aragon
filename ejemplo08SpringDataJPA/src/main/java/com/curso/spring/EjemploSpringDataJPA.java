package com.curso.spring;

import java.util.List;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

import com.curso.spring.config.JavaConfig;
import com.curso.spring.model.business.GestorPeliculas;
import com.curso.spring.model.entity.Pelicula;

public class EjemploSpringDataJPA {
public static AbstractApplicationContext context = null;
	
	public static void main(String[] args) {
		context = new AnnotationConfigApplicationContext(JavaConfig.class);
		GestorPeliculas gestorPeliculas = context.getBean(GestorPeliculas.class);
		Pelicula pelicula = new Pelicula();
		pelicula.setTitulo("El señor de los anillos");
		pelicula.setDirector("Director chuli");
		pelicula.setGenero("Fantasia");
		pelicula.setYear(1997);
		gestorPeliculas.insertar(pelicula);
		Pelicula pelicula2 = new Pelicula();
		pelicula2.setTitulo("Las Dos torres");
		pelicula2.setDirector("Director chuli");
		pelicula2.setGenero("Fantasia");
		pelicula2.setYear(1998);
		gestorPeliculas.insertar(pelicula2);
		List<Pelicula> peliculas = gestorPeliculas.listar();
		mostrarLista(peliculas);
		Pelicula pelicula3 = peliculas.get(0);
		pelicula3.setTitulo(pelicula3.getTitulo().toUpperCase());
		gestorPeliculas.modificar(pelicula3);
		peliculas = gestorPeliculas.listar();
		mostrarLista(peliculas);
		gestorPeliculas.borrar(pelicula3.getId());
		peliculas = gestorPeliculas.listar();
		mostrarLista(peliculas);
		Pelicula pelicula4 = gestorPeliculas.buscar(peliculas.get(0).getId());
		System.out.println("Pelicula encontrada:");
		System.out.println(pelicula4);
		List<Pelicula> tituloAscendente = gestorPeliculas.buscaPorDirectorOrdenadoPorTituloAscendente("Director chuli");
		System.out.println("Peliculas por titulo ascendente");
		mostrarLista(tituloAscendente);
		List<Pelicula> buscaPorMovida = gestorPeliculas.buscaPorMovida("Director chuli");
		System.out.println("buscaPorMovida");
		mostrarLista(buscaPorMovida);
		List<Pelicula> porTitulo = gestorPeliculas.buscaPorTitulo("las dos torres");
		System.out.println("porTitulo");
		mostrarLista(porTitulo);
		context.close();
	}
	private static void mostrarLista(List<Pelicula> peliculas) {
		
		System.out.println("Lista de películas ---------------------");
		for(Pelicula pelicula: peliculas) {
			System.out.println(pelicula);
		}
	}
}


