package com.curso.spring.model.persistence;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.curso.spring.model.entity.Pelicula;
// Esto es opcional
@Repository
public interface PeliculaRepository extends JpaRepository<Pelicula, Integer> {

	
	List<Pelicula> findByTitulo(String titulo);
	
	List<Pelicula> findByTituloOrDirector(String titulo, String director);
	
	//@Query("select p from Pelicula p where p.director=:director");
	@Query("select p from Pelicula p where p.director=?1")
	List<Pelicula> findByMovida(String director);
	
	
	//@Transactional
	@Modifying
	@Query("update Pelicula p set p.year=1999")
	void modificar();
	
	
	// Distinct findDistinctTituloByDirectorOrYear
	
	List<Pelicula> findDistinctTituloByDirector(String director);
	
	// Ignore Case
	
	List<Pelicula> findByTituloIgnoreCase(String titulo);
	
	// OrderBy
	
	List<Pelicula> findByDirectorOrderByTituloAsc(String director);
	List<Pelicula> findByDirectorOrderByTituloDesc(String director);
	
	// Relaciones
	//List<Persona> findByDireccion_CodigoPostal(String codigoPostal);
}
