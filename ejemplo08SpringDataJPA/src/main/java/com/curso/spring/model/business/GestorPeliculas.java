package com.curso.spring.model.business;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.curso.spring.model.entity.Pelicula;
import com.curso.spring.model.persistence.PeliculaRepository;

@Service
public class GestorPeliculas{
	
	@Autowired
	private PeliculaRepository peliculaRepository;
	@Autowired
	public GestorPeliculas() {
		super();
		System.out.println("Creando un gestor de peliculas");
	}
	@Transactional
	public Pelicula insertar(Pelicula pelicula) {
		return peliculaRepository.save(pelicula);
	}
	@Transactional
	public Pelicula modificar(Pelicula pelicula) {
		
		return peliculaRepository.save(pelicula);
	}
	
	public Pelicula buscar(int id) {
		return peliculaRepository.findById(id).get();
	}
	public List<Pelicula> listar(){
		return peliculaRepository.findAll();
	}
	@Transactional
	public void borrar(int id) {
		peliculaRepository.deleteById(id);
	}
	public List<Pelicula> buscaPorTitulo(String titulo){
		return peliculaRepository.findByTituloIgnoreCase(titulo);
	}
	public List<Pelicula> buscaPorMovida(String director){
		return peliculaRepository.findByMovida(director);
	}
	public List<Pelicula> buscaPorDirectorOrdenadoPorTituloAscendente(String director){
		return peliculaRepository.findByDirectorOrderByTituloAsc(director);
	}
}
