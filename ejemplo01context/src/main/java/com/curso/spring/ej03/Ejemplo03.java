package com.curso.spring.ej03;

import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Ejemplo03 {
	private static ApplicationContext context;

	public static void main(String[] args) {
		context = new ClassPathXmlApplicationContext("applicationContext03.xml");
		Persona tomy = context.getBean("tomy",Persona.class);
		System.out.println("Tomy " + tomy.getNombre() + " Edad " + tomy.getEdad() + " Peso " + tomy.getPeso()) ;
		System.out.println("Este es tomy" + tomy);
		Persona alber = context.getBean("alber",Persona.class);
		System.out.println("Alber " + alber.getNombre() + " Edad " + alber.getEdad() + " Peso " + alber.getPeso()) ;
		System.out.println("Este es alber" + alber);
		Persona persona1 = context.getBean("personaPrototype",Persona.class);
		persona1.setNombre("Johnny");
		Persona persona2 = context.getBean("personaPrototype",Persona.class);
		persona2.setNombre("Guille");
		Persona persona3 = context.getBean("personaPrototype",Persona.class);
		persona3.setNombre("Luis Ramón");
		
		List<Persona> listaPersonas = context.getBean("listaPersonas",List.class);
		listaPersonas.add(persona1);
		listaPersonas.add(persona2);
		listaPersonas.add(persona3);
		listaPersonas.add(tomy);
		listaPersonas.add(alber);
		
		otroMetodo();
	}

	private static void otroMetodo() {
		otroMetodoMas();
		
	}

	private static void otroMetodoMas() {
		List<Persona> listaPersonas = context.getBean("listaPersonas",List.class);
		for(Persona p: listaPersonas) {
			System.out.println(p);
		}
		Persona tomy = context.getBean("tomy",Persona.class);
		System.out.println(tomy) ;
		Persona alber = context.getBean("alber",Persona.class);
		System.out.println(alber) ;
		
		Persona persona1 = context.getBean("personaPrototype",Persona.class);
	
		
		System.out.println(persona1) ;
	}
}
