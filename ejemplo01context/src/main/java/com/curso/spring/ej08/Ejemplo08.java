package com.curso.spring.ej08;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Ejemplo08 {
	public static ApplicationContext context = null;
	
	public static void main(String[] args) {
		context = new ClassPathXmlApplicationContext("applicationContext08.xml");
		Persona persona = context.getBean("persona",Persona.class);
		System.out.println(persona);
		System.out.println(persona.getNombre());
		System.out.println(persona.getDireccion().getNombreVia());
		
		
		Cliente cliente = context.getBean("cliente",Cliente.class);
		System.out.println(cliente);
		System.out.println(cliente.getPedido().getImporte());
	}
}
