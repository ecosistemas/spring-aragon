package com.curso.spring.ej01;

public class Mensaje {
	private String cuerpo;
	private String firma;
	
	public String getCuerpo() {
		return cuerpo;
	}
	public void setCuerpo(String cuerpo) {
		this.cuerpo = cuerpo;
	}
	public String getFirma() {
		return firma;
	}
	public void setFirma(String firma) {
		this.firma = firma;
	}
	
	@Override
	public String toString() {
		return "Mensaje [cuerpo=" + cuerpo + ", firma=" + firma + "]";
	}

}
