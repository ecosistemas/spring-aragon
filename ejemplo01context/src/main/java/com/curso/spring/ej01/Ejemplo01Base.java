package com.curso.spring.ej01;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Ejemplo01Base {
	public static ApplicationContext context = null;
	public static void main(String[] args) {
		context = new ClassPathXmlApplicationContext("applicationContext01.xml");
		System.out.println("El contexto ya ha sido cargado");
		
		Mensaje mensajeVacio = (Mensaje)context.getBean("mensajeVacio");
		System.out.println("Contenido del mensaje vacio : " +mensajeVacio);
		
		mensajeVacio.setCuerpo("Quedamos a las 8!");
		mensajeVacio.setFirma("Steve Rogers");
		
		Mensaje otroMensaje = context.getBean("mensajeVacio",Mensaje.class);
		System.out.println("Contenido del mensajeVacio : " + otroMensaje);
		
		Mensaje mensaje = context.getBean("mensajeCumplimentado",Mensaje.class);
		System.out.println("Contenido del mensaje cumplimentado" + mensaje);
		
	}
}
