package com.curso.spring.ej09;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Ejemplo09 {
public static ApplicationContext context = null;
	
	public static void main(String[] args) {
		context = new ClassPathXmlApplicationContext("applicationContext09.xml");
		System.out.println(context.getBean("persona",Persona.class));
		System.out.println(context.getBean("persona2",Persona.class));
		System.out.println(context.getBean("persona3",Persona.class));
	}
}
