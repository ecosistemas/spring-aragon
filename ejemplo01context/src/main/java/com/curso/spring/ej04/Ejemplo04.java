package com.curso.spring.ej04;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Ejemplo04 {
	private static ApplicationContext context;

	public static void main(String[] args) {
		context = new ClassPathXmlApplicationContext("applicationContext04.xml");
		
		Persona tony = context.getBean("tony",Persona.class);
		System.out.println(tony.getDireccion().getNombreVia() + " " + tony.getDireccion().getTipoVia());
		
		Persona diego = context.getBean("diego",Persona.class);
		System.out.println(diego.getDireccion().getNombreVia() + " " + diego.getDireccion().getTipoVia());
		
		Persona persona = new Persona();
		persona.setDireccion(new Direccion());
		
		Persona persona2 = context.getBean("personaPrototype",Persona.class);
	}

	
}
