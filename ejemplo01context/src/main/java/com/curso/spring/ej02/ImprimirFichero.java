package com.curso.spring.ej02;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class ImprimirFichero implements Imprimible {

	@Override
	public void imprimir(String cadena) {
		File file = new File("fichero.txt");
		
		try(PrintWriter pw = new PrintWriter(file)){
			pw.print(cadena);
		} catch (FileNotFoundException e) {
			System.err.println("\nError : " + e.getMessage());
		}

	}

}
