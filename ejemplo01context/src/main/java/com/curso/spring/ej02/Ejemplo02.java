package com.curso.spring.ej02;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Ejemplo02 {
	private static ApplicationContext context;

	public static void main(String[] args) {
		context = new ClassPathXmlApplicationContext("applicationContext02.xml");
		
		Imprimible imprimible = context.getBean("imprimir",Imprimible.class);
		imprimible.imprimir("Hola caracola");
	}
}
