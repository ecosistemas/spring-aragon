package com.curso.spring.ej06;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Ejemplo06 {
	private static ApplicationContext context;

	public static void main(String[] args) {
		context = new ClassPathXmlApplicationContext("applicationContext06.xml");
		System.out.println("CONTEXTO CREADO------");
		context.getBean("tomy",Persona.class);
		context.getBean("alber",Persona.class);
		context.getBean("personaPrototype",Persona.class);
		context.getBean("personaPrototype",Persona.class);
		System.out.println("DESTRUYENDO CONTEXTO------");
		AbstractApplicationContext ctx = (AbstractApplicationContext)context;
		ctx.close();
		
	}

	
}
