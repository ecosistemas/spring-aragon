package com.curso.spring.ej07;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class Colecciones {
	private List<Persona> listaPersonas;
	private Set<String> listaNombres;
	private Map<Integer,Persona> personasPorId;
	public List<Persona> getListaPersonas() {
		return listaPersonas;
	}
	public void setListaPersonas(List<Persona> listaPersonas) {
		this.listaPersonas = listaPersonas;
	}
	public Set<String> getListaNombres() {
		return listaNombres;
	}
	public void setListaNombres(Set<String> listaNombres) {
		this.listaNombres = listaNombres;
	}
	public Map<Integer, Persona> getPersonasPorId() {
		return personasPorId;
	}
	public void setPersonasPorId(Map<Integer, Persona> personasPorId) {
		this.personasPorId = personasPorId;
	}
	
}
