package com.curso.spring.ej07;

public class Persona {
	private String nombre;
	private int edad;
	private double peso;
	
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getEdad() {
		return edad;
	}
	public void setEdad(int edad) {
		this.edad = edad;
	}
	public double getPeso() {
		return peso;
	}
	public void setPeso(double peso) {
		this.peso = peso;
	}
	public void inicializar() {
		System.out.println("Construyendo con El contexto de Spring -" + this.nombre );
	}
	
	public void destruir() {
		System.out.println("Destruyendo desde el contexto de Spring - Arrgghhhh!! " + this.nombre);
	}
	@Override
	public String toString() {
		return "Persona [nombre=" + nombre + ", edad=" + edad + ", peso=" + peso + "]";
	}
	
}
