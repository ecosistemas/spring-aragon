package com.curso.spring.ej07;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Ejemplo07 {
	private static ApplicationContext context;

	public static void main(String[] args) {
		context = new ClassPathXmlApplicationContext("applicationContext07.xml");
		List listaNombres = context.getBean("listaNombres",List.class);
		System.out.println(listaNombres);
		List listaPersonas = context.getBean("listaPersonas",List.class);
		System.out.println(listaPersonas);
		System.out.println(context.getBean("mapaPersonas",Map.class));
		System.out.println(context.getBean("setPersonas",Set.class));
		
	}

}
