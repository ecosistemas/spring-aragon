package com.curso.spring.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
//@Scope("request")
public class ControladorSaludos {

	@RequestMapping("verFormularioSaludo")
	public ModelAndView verFormularioSaludo() {
		ModelAndView mav = new ModelAndView("formularioSaludo");
		return mav;
	}
	
	@RequestMapping(path = "saludo", method = RequestMethod.POST)
	public ModelAndView metodoSaludo(@RequestParam("nombre") String nombre,
			HttpServletRequest request,
			HttpSession session,
			ModelAndView mav
			) {
		String saludo = "Hola " + nombre;
		// redirect:verSaludos
		mav.setViewName("saludo");
		mav.addObject("saludo", saludo);
		return mav;
	}
}
