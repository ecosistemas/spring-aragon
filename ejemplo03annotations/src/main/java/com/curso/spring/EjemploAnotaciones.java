package com.curso.spring;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.curso.spring.model.business.GestorPeliculas;
import com.curso.spring.model.entity.Pelicula;

public class EjemploAnotaciones {
public static AbstractApplicationContext context = null;
	
	public static void main(String[] args) {
		context = new ClassPathXmlApplicationContext("applicationContext.xml");
		GestorPeliculas gestorPeliculas = context.getBean(GestorPeliculas.class);
		gestorPeliculas.insertar(new Pelicula());
		context.close();
	}
}
