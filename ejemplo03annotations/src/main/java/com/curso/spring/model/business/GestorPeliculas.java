package com.curso.spring.model.business;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.curso.spring.model.entity.Pelicula;
import com.curso.spring.model.persistence.PeliculaDao;
import com.curso.spring.util.Logger;

@Service
public class GestorPeliculas {
	
	@Autowired
	private PeliculaDao peliculaDao;
	@Autowired
	@Qualifier("logger")
	private Logger logger_;
	@Autowired
	@Qualifier("loggerError")
	private Logger loggerError_;
	
	public GestorPeliculas() {
		super();
		System.out.println("Creando un gestor de peliculas");
	}
	public void insertar(Pelicula pelicula) {
		// Negocio!
		System.out.println("Lanzando loggers");
		logger_.escribir("Lanzando operacion de guardado de pelicula");
		loggerError_.escribir("Cuidado!");
		peliculaDao.save(pelicula);
	}
	
	
}
