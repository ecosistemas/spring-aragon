package com.curso.spring.model.persistence;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import com.curso.spring.model.entity.Pelicula;

@Repository
@Scope("singleton") // Implicito
public class PeliculaDao {
	
	@Autowired
	//@Qualifier("dataSource") // id del bean!
	private DataSource dataSource;// NULL

	
	public void save(Pelicula pelicula) {
		Connection connection = null;
		try {
			connection = dataSource.getConnection();
			System.out.println(connection);
		} catch (SQLException e) {
			System.err.println("\n"+e.getMessage());
		}finally {

			try {
				connection.close();
			} catch (SQLException e) {
				
				System.err.println("\n"+e.getMessage());
			}
		}
	}
}
