package com.curso.spring.lifecycle;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

public class BeanCicloDeVida implements InitializingBean, DisposableBean{

	private String dato;
	
	
	
	public void setDato(String dato) {
		this.dato = dato;
	}



	public BeanCicloDeVida() {
		super();
		System.out.println("Instanciación del BeanCicloDeVida");
		System.out.println("Dato:" + dato);
	}



	public void afterPropertiesSet() throws Exception {
		System.out.println("BeanCicloDeVida.afterPropertiesSet");
		System.out.println("Dato:" + dato);
		
	}



	public void destroy() throws Exception {
		System.out.println("BeanCicloVida.destroy");
		
	}
	
//	@PostConstruct
//	public void inicializarJEE() {
//		System.out.println("Inicializando desde JEE");
//	}
//	@PreDestroy
//	public void destruirJEE() {
//		System.out.println("Destruyendo desde JEE");
//	}
}
