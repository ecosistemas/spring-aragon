package com.curso.spring.lifecycle;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.curso.spring.util.Logger;

public class EjemploLifecycle {
public static AbstractApplicationContext context = null;
	
	public static void main(String[] args) {
		context = new ClassPathXmlApplicationContext("applicationContext.xml");
		BeanCicloDeVida bean = context.getBean(BeanCicloDeVida.class);
		Logger logger = context.getBean("logger",Logger.class);
		Logger loggerError = context.getBean("loggerError",Logger.class);
		logger.escribir("Hola desde el log");
		loggerError.escribir("Zasca!");
		BeanQueSabeComoSeLlama llama = context.getBean(BeanQueSabeComoSeLlama.class);
		llama.saludar();
		context.close();
		
	}
	
	
}
