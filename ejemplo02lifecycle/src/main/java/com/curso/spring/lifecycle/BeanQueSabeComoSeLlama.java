package com.curso.spring.lifecycle;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import com.curso.spring.util.Logger;

public class BeanQueSabeComoSeLlama implements BeanNameAware,InitializingBean,ApplicationContextAware{

	private String name;
	private ApplicationContext context;
	@Override
	public void setBeanName(String name) {
		this.name = name;
		System.out.println("Spring inyecta mi nombre!");
		System.out.println("Nombre: " + name);
		
	}
	@Override
	public void afterPropertiesSet() throws Exception {
		System.out.println("Después de inyectar las properties!");
		System.out.println("Nombre; " + this.name);
		Logger logger = context.getBean("logger",Logger.class);
		logger.escribir("Yo soy " + this.name);
		
	}
	public void saludar() {
		System.out.println("Hola " + this.name);
	}
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.context = applicationContext;
		
	}
	

}
